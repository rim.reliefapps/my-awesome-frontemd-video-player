FROM node:16 as builder
WORKDIR /app
COPY . .
RUN npm i && npm run build

FROM nginx:alpine
WORKDIR /usr
#RUN rm -rf ./*
COPY --from=builder app/dist/video-player-frontend /usr/share/nginx/html
#ENTRYPOINT ["nginx","g","daemon"]
